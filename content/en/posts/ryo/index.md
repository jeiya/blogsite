---
author: Vin Gumapos 
title: An Excellent Day in September by Vin Gumapos
date: 2023-09-27
description: A poem by Vin Gumapos
toc: false
---

<p align="center">
    <img src="ryo.webp">
</p>


<p align="center">
Blue as the morning in September,
<br/>
Fluff like cumulus clouds,
<br/>
Citrine glass reflecting the ember,
<br/>
Excellent! I shout out loud,

<p align="center">
As I travel through land,
<br/>
Blades of grass struck me,
<br/>
Little did I know,
<br/>
I stepped on a band.
<br/>

<p align="center">
Oh, my Ryo,
<br/>
What have I done?
<br/>
What in Ohio,
<br/>
Had just begun?
<br/>

<p align="center">
Dear, why are you here?
<br/>
I told you already,
<br/>
Didn’t you hear?
<br/>
Pack up and get ready.
<br/>

<p align="center">
Pick up the pace,
<br/>
Reach for your bass,
<br/>
Let’s start jamming,
<br/>
And get the people clapping.
<br/>

<p align="center">
    <img src="ryo2.webp">
</p>
