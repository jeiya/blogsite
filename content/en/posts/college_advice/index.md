---
author: Joseph Aniag
title: College Course Advice - CE, CS, and IT.
date: 2023-09-26
description: Differences between CE, CS, and IT. Pick the right course for you.
toc: false
---

# Computer Engineering, Computer Science, and Information Technology: Which is right for you?
<br>

---

In pursuing higher education, one may feel overwhelemed at the mutlitude of choices; especially when one does not have enough knowledge on which is best suited for them. When it comes to people who are generally knowledgeable and experienced in the field of technology, they choose between these three courses the most: Computer Engineering, Computer Science, and Information Technology. In this blog post, we will address the concerns of individuals who are unsure of which path to take for their college courses, what you can expect from each field, and highlighting the technical differences of CE, CS, and IT.

## COMPUTER ENGINEERING
![Landscape](CE.webp)
<br>

Of the three, this course is the closest to Electrical Engineering, as this will teach you the ins and outs of computer hardware, circuitry, and interaction between software and hardware systems.
1. **Emphasis on Hardware**: this is typically where you design and analyze processors, motherboards, and other electrical components necessary to build a functioning computer. Examples of these are: GPU, CPU, motherboards, RAM, and hard drives (hdd/ssd).
2. **Circuit Design**: this is where you develop skills in making efficient and stable electronic systems, down to the individual transistors and other electrical components. For students, their first exposure to this maybe the **Arduino** (microcontroller) and **Raspberry Pi** (microprocessors/single-board-computer).
3. **Low-Level Programming**: when one calls a programming language as "low-level", the software interacts with the hardware itself. While programming languages normally only interact with the operating system and software, this type of language can control every part of a computer, down to its electronic components. Well known low-level languages are: C, Java, Rust, Fortran, and Assembly.
## COMPUTER SCIENCE
![Landscape](CS.jpg)
<br>

This course is highly theoretical and reliant mostly on mathematics and logic. Computer Science is a very broad course that covers a wide range of topics; from software development, game development, app development to algorithms, data science, and data structures.
1. **Theory and Mathematics**: CS places a strong emphasis on the science behind computing and how software runs and works.
2. **Programming**: within this course, you will be introduced to the fundamentals of different programming languages and the different operations required to create a full and working program. While you will usually hear that python is the best popular beginner language, most institutions will have you learn C or Java first as they will instill key concepts and habits into your software code to make them run efficiently.
3. **Algorithms**: is a procedure for performing data computations, allowing one to automatically perform inhuman calculations on large data sets at a large scale. This field is partically useful in AI, research, finance, and other areas where complex computations are needed to be performed everyday.
## INFORMATION TECHNOLOGY
![Landscape](IT.jpg)
<br>

A field that focuses on the practical application of technology in a corporate/business environment. IT professionals act as the ones who bridge the gap between normal users/workers and evolving complex technological systems of an organization, critical for day-to-day business operations.
1. **IT Support**: on-demand support for individuals or groups who are not technically-inclined. Their tasks range from simple installation of a web browser, and application crashes to hardware troubleshooting and IT resouce management.
2. **Business and Management**: IT professionals require firm communication skills and understanding of how to align the technology with business objectives/models. Strategic implementation of new or existing technology to the workoffice. Negotiate contracts, evaluate performance and value of third-party services, and ensuring that services meet the agreed-upon service-level agreement. The highest corporate position of this field is the Chief Technology Officer (CTO).
3. **System Administration**: particularly manage and administer servers, employee work machines, software, network, and security of a building, department, or organization. This field performs a large scope of tasks that range from trivial router setups, newly-employed worker account creation, and server-client connection to database administration, service hosting, data backup and recovery, system and network security, and maintenance/monitoring.

### Personal Advice
* While choosing one of these three and picking that specific course is ideal, for IT it is usually necessary to have a Bachelor's in computer science for more job opportunities, unless you have years of experience with the employers current technologies and years of experience as an IT.
* IT is still a very secure job as while most organizations dont employ software development or the like, IT is more than likely an already existing department within the organization because most organizations in the 21st century are using some form of technology; whether it be printers, internet, or a simple website server.
* All 3 courses are definitely more of the "learn it on your own" type. Technology is rapidly evolving and we can't expect all institutions to catch-up and update their curriculum, hence it is important to keep yourself updated and keep learning outside of these institutions. In addition, professors will most likely teach you the fundamentals but it will be up to you to go further and understand the more advanced topics and details.
