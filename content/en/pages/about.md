---
title: About Me
description: 'My Story'
author: Joseph Aniag
---

Acknowledgments
* https://github.com/gohugoio
* https://github.com/hugo-sid/hugo-blog-awesome
* https://github.com/yuin/goldmark
* https://github.com/alecthomas/chroma
* https://github.com/spf13/cobra
* https://github.com/muesli/smartcrop
* https://github.com/spf13/viper

### Profile Summary

### Professional Highlights

### What Sets Me Apart
